package net.onpu_tamago.test.animationextest;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;

public class TranslateAnimationEx extends TranslateAnimation implements
		AnimationListener {

	private AnimationListener mListener;
	private View mView;
	private int mFromX;
	private int mFromY;
	private int mToX;
	private int mToY;

	public TranslateAnimationEx(View animatedView, int dulation, int toXValue,
			int toYValue) {
		super(0, toXValue, 0, toYValue);
		setDuration(dulation);
		mView = animatedView;
		mFromX = mView.getLeft();
		mFromY = mView.getTop();
		mToX = toXValue;
		mToY = toYValue;
		setAnimationListener(this);
	}

	public TranslateAnimationEx(View animatedView, int dulation,
			int fromXValue, int toXValue, int fromYValue, int toYValue) {
		super(fromXValue - animatedView.getLeft(), toXValue
				- animatedView.getLeft(), fromYValue - animatedView.getTop(),
				toYValue - animatedView.getTop());
		setDuration(dulation);
		Log.d("", "x:" + fromXValue + " -> " + toXValue + ":y:" + fromYValue
				+ " -> " + toYValue);
		mView = animatedView;
		mFromX = fromXValue;
		mFromY = fromYValue;
		mToX = toXValue - fromXValue;
		mToY = toYValue - fromYValue;
		setAnimationListener(this);
	}

	public void animate() {
		mView.startAnimation(this);
	}

	@Override
	public void setAnimationListener(AnimationListener listener) {
		if (listener.equals(this)) {
			super.setAnimationListener(listener);
		} else {
			mListener = listener;
		}
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		int x = mFromX + mToX;
		int y = mFromY + mToY;
		Log.d("", "x:" + x + "\ty:" + y);
		mView.layout(x, y, x + mView.getWidth(), y + mView.getHeight());
		mView.setAnimation(null);
		mView.setClickable(true);
		if (mListener != null) {
			mListener.onAnimationEnd(animation);
		}
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		if (mListener != null) {
			mListener.onAnimationRepeat(animation);
		}
	}

	@Override
	public void onAnimationStart(Animation animation) {
		mView.setVisibility(View.VISIBLE);
		mView.layout(mFromX, mFromY, mFromX + mView.getWidth(),
				mFromY + mView.getHeight());
		mView.setClickable(false);
		if (mListener != null) {
			mListener.onAnimationStart(animation);
		}
	}

}
