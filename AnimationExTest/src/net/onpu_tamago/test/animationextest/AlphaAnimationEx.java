package net.onpu_tamago.test.animationextest;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class AlphaAnimationEx extends AlphaAnimation implements
		AnimationListener {

	private AnimationListener mListener;
	private View mView;
	private float mTo;
	private View mOldView;

	public AlphaAnimationEx(View animatedView, int duration, float fromAlpha,
			float toAlpha) {
		super(fromAlpha, toAlpha);
		this.mView = animatedView;
		this.mTo = toAlpha;
		setDuration(duration);
		setAnimationListener(this);
	}

	public AlphaAnimationEx(View animatedView, View oldView, int duration,
			float fromAlpha, float toAlpha) {
		super(fromAlpha, toAlpha);
		this.mView = animatedView;
		this.mOldView = oldView;
		this.mTo = toAlpha;
		setDuration(duration);
		setAnimationListener(this);
	}

	public void animate() {
		mView.startAnimation(this);
	}

	@Override
	public void setAnimationListener(AnimationListener listener) {
		if (listener.equals(this)) {
			super.setAnimationListener(listener);
		} else {
			mListener = listener;
		}
	}

	public void setView(View view) {
		this.mView = view;
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		mView.setVisibility(mTo <= 0.0f ? View.INVISIBLE : View.VISIBLE);
		mView.setClickable(true);
		if (mOldView != null) {
			mOldView.setVisibility(View.GONE);
		}
		mView.setAnimation(null);
		if (mListener != null) {
			mListener.onAnimationEnd(animation);
		}
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		if (mListener != null) {
			mListener.onAnimationRepeat(animation);
		}
	}

	@Override
	public void onAnimationStart(Animation animation) {
		mView.setVisibility(View.VISIBLE);
		mView.setClickable(false);
		if (mListener != null) {
			mListener.onAnimationStart(animation);
		}
	}

}
