package net.onpu_tamago.test.animationextest;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;

public class MainActivity extends Activity implements OnTouchListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		FrameLayout relative_frame = (FrameLayout) findViewById(R.id.relative_frame);
		FrameLayout abstract_frame = (FrameLayout) findViewById(R.id.abstract_frame);
		relative_frame.setOnTouchListener(this);
		abstract_frame.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			final ImageView relative_anime = (ImageView) findViewById(R.id.relative_anime);
			final ImageView abstract_anime = (ImageView) findViewById(R.id.abstract_anime);
			Spinner animate_type = (Spinner) findViewById(R.id.animate_type);
			switch (animate_type.getSelectedItemPosition()) {
			case 0: {
				final int startx = abstract_anime.getLeft();
				final int starty = abstract_anime.getTop();
				final int movex = (int) event.getX() - startx;
				final int movey = (int) event.getY() - starty;
				new TranslateAnimationEx(relative_anime, 1000, movex, movey)
						.animate();
				new TranslateAnimationEx(abstract_anime, 1000, startx, movex
						+ startx, starty, movey + starty).animate();
				Handler h = new Handler();
				h.postDelayed(new Runnable() {

					@Override
					public void run() {
						new TranslateAnimationEx(relative_anime, 1000, -movex,
								-movey).animate();
						new TranslateAnimationEx(abstract_anime, 1000,
								abstract_anime.getLeft(), startx,
								abstract_anime.getTop(), starty).animate();
					}
				}, 1500);

			}
				break;
			case 1: {
				final float alpha2 = (float) Math.random();

				new AlphaAnimationEx(relative_anime, 1000, 1.0f, 0.0f)
						.animate();
				new AlphaAnimationEx(abstract_anime, 1000, 1.0f, alpha2)
						.animate();
				Handler h = new Handler();
				h.postDelayed(new Runnable() {

					@Override
					public void run() {
						new AlphaAnimationEx(relative_anime, 1000, 0.0f, 1.0f)
								.animate();
						new AlphaAnimationEx(abstract_anime, 1000, alpha2, 1.0f)
								.animate();
					}
				}, 1500);
			}
				break;

			default:
				break;
			}
			break;

		default:
			break;
		}
		return false;
	}
}
